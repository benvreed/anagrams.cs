using System;

namespace Anagrams
{
	class Anagram
	{
		/// <summary>
		/// The entry point of the program, where the program control starts and ends.
		/// </summary>
		/// <param name='args'>
		/// The command-line arguments.
		/// </param>
		public static void Main (string[] args)
		{
			// display the purpose of the Application
			Console.Write ("This program determines if two input strings are anagrams.\n");
			// prompt for input
			GetInput();
		}

		/// <summary>
		/// Gets the input.
		/// </summary>
		public static void GetInput ()
		{
			string str1, str2;

			// store the first string
			Console.Write ("#1: ");
			str1 = Console.ReadLine ();

			// store the second string
			Console.Write ("#2: ");
			str2 = Console.ReadLine ();

			// check and return if anagram
			if (IsAnagram (str1, str2)) {
				Console.Write ("\"{0}\" is an anagram of \"{1}\"\n", str1, str2);
			} else {
				Console.Write ("\"{0}\" is not an anagram of \"{1}\"\n", str1, str2);
			}
		}

		/// <summary>
		/// Determines if first and second are anagrams.
		/// </summary>
		/// <returns>
		/// <c>true</c> if is anagram; otherwise, <c>false</c>.
		/// </returns>
		/// <param name='first'>
		/// First string.
		/// </param>
		/// <param name='second'>
		/// Second string.
		/// </param>
		public static bool IsAnagram (string first, string second)
		{
			// sanitize and sort the strings
			char[] str1 = CreateSanitizedAndSortedArrayFromInput (first);
			char[] str2 = CreateSanitizedAndSortedArrayFromInput (second);

			// if they are the same length
			if (str1.Length == str2.Length) {
				// check each character to verify that is is the same
				for (int i = 0; i < str1.Length; i++)
				{
					if (str1[i] != str2[i])
					{
						// otherwise, return false
						return false;
					}
				}
				// return true if this point is reached
				return true;
			}
			// if they are not the same length, return false
			return false;
		}
		
		/// <summary>
		/// Safely sanitize a string.
		/// Remove unwanted characters, such as: " ", "\'", "\t", ",", ":", "!", ";".
		/// </summary>
		/// <returns>
		/// The sanitized string.
		/// </returns>
		/// <param name='input'>
		/// Input.
		/// </param>
		public static string SafelySanitizeString (string input)
		{
			// use the sanitizers[] to sanitize the string
			string[] sanitizers = {" ", "\'", "\t", ",", ":", "!", "?", ".", ";"};
			string retStr = input.ToUpper ();

			foreach (string s in sanitizers) {
				retStr = retStr.Replace(s, "");
			}
			
			return retStr;
		}

		/// <summary>
		/// Creates a sanitized and sorted array from input.
		/// </summary>
		/// <returns>
		/// The sanitized and sorted array from input.
		/// </returns>
		/// <param name='input'>
		/// Input.
		/// </param>
		private static char[] CreateSanitizedAndSortedArrayFromInput (string input)
		{
			// create a sorted char array
			string newStr = SafelySanitizeString (input);
			char[] ins = ToSortedCharArray(newStr);
			return ins;
		}

		/// <summary>
		/// Convert a String to a sorted char array.
		/// </summary>
		/// <returns>
		/// The sorted char array.
		/// </returns>
		/// <param name='input'>
		/// Input.
		/// </param>
		private static char[] ToSortedCharArray (string input)
		{
			// convert the array to ascii integers
			char[] arr = input.ToCharArray ();
			int[] ascii = new int[arr.Length];
			for (int i = 0; i < arr.Length; i++) {
				ascii[i] = (int)arr[i];
			}

			// sort using selection sort and convert it back to char
			int[] results = SelectionSortByASCII(ascii);
			for (int i = 0; i < results.Length; i++) {
				arr[i] = (char)results[i];
			}

			return arr;
		}

		/// <summary>
		/// Selections sort algorithm using ASCI.
		/// </summary>
		/// <returns>
		/// The sort by ASCI.
		/// </returns>
		/// <param name='numbers'>
		/// Numbers array.
		/// </param>
		private static int[] SelectionSortByASCII (int[] numbers)
		{
			// implementation of selection sort algorithm
			// find the minimum
			int min = 0;
			for (int i = 0; i < numbers.Length; i++) {
				min = i;
				for (int j = i; j < numbers.Length; j++) {
					if (numbers[j] < numbers[min])
					{
						min = j;
					}
				}
				// swap with the side
				Swap(ref numbers, i, min);
			}
			return numbers;
		}

		/// <summary>
		/// Swap the specified numbers, pos1 and pos2.
		/// </summary>
		/// <param name='numbers'>
		/// Numbers.
		/// </param>
		/// <param name='pos1'>
		/// Pos1.
		/// </param>
		/// <param name='pos2'>
		/// Pos2.
		/// </param>
		private static void Swap (ref int[] numbers, int pos1, int pos2)
		{
			// swap 2 positions using a temporary variable, temp
			int temp = numbers[pos1];
			numbers[pos1] = numbers[pos2];
			numbers[pos2] = temp;
		}
	}
}
