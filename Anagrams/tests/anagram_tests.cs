using System;
using NUnit.Framework;

namespace Anagrams
{
	[TestFixture()]
	public class AnagramTests
	{
		/// <summary>
		/// Tests if anagram detected without special conditions.
		/// </summary>
		[Test()]
		public void TestAnagramDetectedWithoutSpecialConditions ()
		{
			string str1 = "raw";
			string str2 = "war";

			Assert.IsTrue(Anagram.IsAnagram(str1, str2));
		}

		/// <summary>
		/// Tests if anagram not detected without special conditions.
		/// </summary>
		[Test()]
		public void TestAnagramNotDetectedWithoutSpecialConditions ()
		{
			string str1 = "battle";
			string str2 = "ballet";

			Assert.IsFalse(Anagram.IsAnagram(str1, str2));
		}

		/// <summary>
		/// Tests the sanitization method.
		/// </summary>
		[Test()]
		public void TestSanitizationMethod ()
		{
			string input = "raw candle";
			string expected = "rawcandle";
			string actual = Anagram.SafelySanitizeString(input);
			Assert.AreEqual(expected.Length, actual.Length);
		}

		/// <summary>
		/// Tests if anagram detected with spaces.
		/// </summary>
		[Test()]
		public void TestAnagramDetectedWithSpaces ()
		{
			string str1 = "astronomers";
			string str2 = "no more stars";
			
			Assert.IsTrue(Anagram.IsAnagram(str1, str2));
		}

		/// <summary>
		/// Tests if anagram not detected with spaces.
		/// </summary>
		[Test()]
		public void TestAnagramNotDetectedWithSpaces ()
		{
			string str1 = "battle";
			string str2 = "ballet";
			
			Assert.IsFalse(Anagram.IsAnagram(str1, str2));
		}

		/// <summary>
		/// Tests if anagram detected with case change.
		/// </summary>
		[Test()]
		public void TestAnagramDetectedWithCaseChange ()
		{
			string str1 = "cinema";
			string str2 = "Iceman";
			
			Assert.IsTrue(Anagram.IsAnagram(str1, str2));
		}

		/// <summary>
		/// Tests if anagram not detected with case change.
		/// </summary>
		[Test()]
		public void TestAnagramNotDetectedWithCaseChange ()
		{
			string str1 = "cinema 3";
			string str2 = "aM 3";
			
			Assert.IsFalse(Anagram.IsAnagram(str1, str2));
		}

		/// <summary>
		/// Tests if anagram detected with spaces and cases.
		/// </summary>
		[Test()]
		public void TestAnagramDetectedWithSpacesAndCases ()
		{
			string str1 = "I fear to think I'm here";
			string str2 = "I THINK, therefore I AM!";
			
			Assert.IsTrue(Anagram.IsAnagram(str1, str2));
		}
		
		/// <summary>
		/// Tests if anagram not detected with spaces and cases.
		/// </summary>
		[Test()]
		public void TestAnagramNotDetectedWithSpacesAndCases ()
		{
			string str1 = "Henceforth my name is Bob.";
			string str2 = "Move FORWARD towards the CAKE!";

			Assert.IsFalse(Anagram.IsAnagram(str1, str2));
		}

		/// <summary>
		/// Tests the selection sort implementation.
		/// </summary>
		[Ignore()]
		public void TestSelectionSort ()
		{
			int[] original = { 5, 8, 7, 9, 6, 4, 1, 3, 2 };
			int[] expected = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
			bool sorted = true;
//			int[] actual = Anagram.SelectionSortByASCII (original);
//
//			for (int i = 0; i < actual.Length; i++) {
//				if (actual[i] != expected[i]) {
//					sorted = false;
//				}
//			}

			Assert.IsTrue(sorted);
		}
	}
}

